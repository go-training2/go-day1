package controllers

import (
	"encoding/json"
	models "training-day1/models"
)

func GetBookController(book models.Book) (string, error) {
	bytes, _ := json.Marshal(book)

	return string(bytes), nil
}

func GetBooksController(book []models.Book) (string, error) {
	bytes, _ := json.Marshal(book)

	return string(bytes), nil
}
