package controllers

import (
	"encoding/json"
)

func Filecount() (string, error) {
	fileCount := map[string]int{
		"Inter": 10,
		"Amd":   20,
	}
	bytes, _ := json.Marshal(fileCount)
	return string(bytes), nil
}
