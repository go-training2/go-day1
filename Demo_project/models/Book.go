package models

type Book struct {
	Bookname string
	Price    int
	Discount int
}

func BookContructor() (Book, error) {
	book := Book{
		Bookname: "harry potter and the cursed child",
		Price:    200,
		Discount: 120,
	}
	return book, nil
}

func BooksConstructor() ([]Book, error) {
	books := []Book{
		Book{
			Bookname: "harry potter and the cursed child",
			Price:    200,
			Discount: 120,
		},
		Book{
			Bookname: "Fantastic beat and where to find them",
			Price:    300,
			Discount: 220,
		},
		Book{
			Bookname: "Fantastic beat and secret of dunberdle",
			Price:    300,
			Discount: 220,
		},
	}
	return books, nil
}
