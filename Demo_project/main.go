// package
package main

// import
import (
	"bufio"
	"fmt"
	"os"
	controllers "training-day1/controllers"
	models "training-day1/models"
)

// type User struct {
// 	Username string
// 	Password string
// 	Name     string
// 	Lastname string
// 	IsActive
// }

// type MainUser struct {
// 	Username string
// 	Password string
// 	Name     string
// 	Lastname string
// }

// func UserContructor() (User,string,error) {
// 	user := User{
// 		Username : "zratchaphoom.b",
// 		Password : "R@tDev1234",
// 		Name : "Ratchaphoom",
// 		Lastname : "Boonnaka",
// 	}
// 	return user,"sucess",nil
// }

// function
func main() {

	username := bufio.NewReader(os.Stdin)
	fmt.Print("Enter text: ")
	text, _ := username.ReadString('\n')
	fmt.Println(text)

	user := models.UserContructor()
	fmt.Println(user)

	cpu, err := controllers.Filecount()
	fmt.Println(cpu)
	if err != nil {

	}

	book, _ := models.BookContructor()
	bookVal, err := controllers.GetBookController(book)
	fmt.Println(bookVal)

	books, _ := models.BooksConstructor()
	// booksVal, err := controllers.GetBooksController(books)

	// loop

	for index, val := range books {
		fmt.Println(index, string(val.Bookname))
	}

}
