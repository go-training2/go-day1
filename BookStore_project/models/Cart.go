package models

import (
	"time"
)

type Cart struct {
	Id         int
	Username   string
	CartList   interface{}
	CreateDate time.Duration
	UpdateDate time.Duration
}
