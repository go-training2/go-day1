package models

type Users struct {
	Username string
	Password string
	Name     string
	Lastname string
	Old      int
	IsActive bool
}
