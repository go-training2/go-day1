package models

type Responsemodel struct {
	Code  int
	Message  string
	Data  interface{}
}