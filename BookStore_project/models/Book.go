package models

type Book struct {
	Id               int
	Bookname         string
	Price            float64
	Discount         float64
	Discount_percent float64
	IsActive         bool
}
