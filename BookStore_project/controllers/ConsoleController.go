package controllers

import (
	models "bookstore-project/models"
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func Loginconsolectrl() (models.Login, error) {
	username := bufio.NewReader(os.Stdin)
	fmt.Print("Enter username : ")
	usernametxt, _ := username.ReadString('\n')
	usernametxt = strings.Replace(usernametxt, "\n", "", -1)

	password := bufio.NewReader(os.Stdin)
	fmt.Print("Enter password : ")
	passwordtxt, _ := password.ReadString('\n')
	passwordtxt = strings.Replace(passwordtxt, "\n", "", -1)

	return models.Login{
		Username: usernametxt,
		Password: passwordtxt,
	}, nil
}

func Registerconsolectrl() (models.Users, error) {
	username := bufio.NewReader(os.Stdin)
	fmt.Print("Enter username : ")
	usernametxt, _ := username.ReadString('\n')
	usernametxt = strings.Replace(usernametxt, "\n", "", -1)

	password := bufio.NewReader(os.Stdin)
	fmt.Print("Enter password : ")
	passwordtxt, _ := password.ReadString('\n')
	passwordtxt = strings.Replace(passwordtxt, "\n", "", -1)

	name := bufio.NewReader(os.Stdin)
	fmt.Print("Enter name : ")
	nametxt, _ := name.ReadString('\n')
	nametxt = strings.Replace(nametxt, "\n", "", -1)

	lastname := bufio.NewReader(os.Stdin)
	fmt.Print("Enter lastname : ")
	lastnametxt, _ := lastname.ReadString('\n')
	lastnametxt = strings.Replace(lastnametxt, "\n", "", -1)

	old := bufio.NewReader(os.Stdin)
	fmt.Print("Enter old : ")
	oldtxt, _ := old.ReadString('\n')
	oldtxt = strings.Replace(oldtxt, "\n", "", -1)
	oldInt, err := strconv.Atoi(oldtxt)
	return models.Users{
		Username: usernametxt,
		Password: passwordtxt,
		Name:     nametxt,
		Lastname: lastnametxt,
		Old:      oldInt,
		IsActive: true,
	}, err
}

func Bookconsolectrl() (models.Book, error) {
	bookname := bufio.NewReader(os.Stdin)
	fmt.Print("Enter bookname : ")
	booknametxt, _ := bookname.ReadString('\n')
	booknametxt = strings.Replace(booknametxt, "\n", "", -1)

	price := bufio.NewReader(os.Stdin)
	fmt.Print("Enter price : ")
	pricetxt, _ := price.ReadString('\n')
	pricetxt = strings.Replace(pricetxt, "\n", "", -1)
	priceD, err := strconv.ParseFloat(pricetxt, 64)
	if err != nil {
		fmt.Println(err)
	}

	discount := bufio.NewReader(os.Stdin)
	fmt.Print("Enter discount : ")
	discounttxt, _ := discount.ReadString('\n')
	discounttxt = strings.Replace(discounttxt, "\n", "", -1)
	discountD, err := strconv.ParseFloat(discounttxt, 64)
	if err != nil {
		fmt.Println(err)
	}

	discountpercent := bufio.NewReader(os.Stdin)
	fmt.Print("Enter discountpercent : ")
	discountpercenttxt, _ := discountpercent.ReadString('\n')
	discountpercenttxt = strings.Replace(discountpercenttxt, "\n", "", -1)
	discountpercentD, err := strconv.ParseFloat(discountpercenttxt, 64)
	if err != nil {
		fmt.Println(err)
	}

	return models.Book{
		Bookname:         booknametxt,
		Price:            priceD,
		Discount:         discountD,
		Discount_percent: discountpercentD,
		IsActive:         true,
	}, err
}

func Programconsolectrl() (int, error) {
	programe := bufio.NewReader(os.Stdin)
	fmt.Print("Enter program : ")
	programtxt, _ := programe.ReadString('\n')
	programtxt = strings.Replace(programtxt, "\n", "", -1)
	programInt, err := strconv.Atoi(programtxt)
	return programInt, err
}
